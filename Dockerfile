FROM alpine:latest

WORKDIR /

ARG LDC_VERSION=1.13.0
ARG DMD_VERSION=2.087.0
ARG ARCH=x86_64
ARG GDC_VERSION=2.068.2
ARG DUB_VERSION=1.16.0
ARG USER=meson-worker

ARG BUILD_DEPS="flex bison libexecinfo"
    

RUN apk update && apk add --no-cache \
    xz \
    the_silver_searcher \
    alpine-sdk \
    llvm5 \
    musl-dev \
    curl \
    tzdata \
    openssl \
    zsh \
    unzip \
    git \
    meson \
    ${BUILD_DEPS}




RUN cd / && curl -fsS -L -o "/ldc.tar.xz" "https://github.com/ldc-developers/ldc/releases/download/v${LDC_VERSION}/ldc2-${LDC_VERSION}-alpine-linux-${ARCH}.tar.xz" && \
    tar xf /ldc.tar.xz && \
    mv "/ldc2-${LDC_VERSION}-alpine-linux-${ARCH}" "/ldc" && \
    rm -rf /ldc.tar.xz



# Dub release linux x86_64 doesnt work. just going to use the older one that comes with ldc
# RUN mkdir /dub && \
#    curl -fsS -L -o "/dub.tar.gz" "https://github.com/dlang/dub/releases/download/v${DUB_VERSION}/dub-v${DUB_VERSION}-linux-${ARCH}.tar.gz" && \
#    tar -C "/dub" -xvf "/dub.tar.gz" && \
#    ln -s /dub/dub /usr/local/bin && \
#    rm -rf "/dub.tar.gz" && \
#    chown root:root -R /dub

ENV \
    PATH="/ldc/bin:/usr/bin:/usr/local/bin:$PATH" 

# Add user for application
RUN addgroup -g 1000 ${USER}
RUN adduser -D -u 1000 -s /bin/zsh -G ${USER} -G wheel ${USER}

RUN mkdir /home/$USER/.ldc
RUN chown ${USER}:${USER} /home/$USER/.ldc

COPY ldc2.conf /home/$USER/.ldc/ldc2.conf
RUN chown ${USER}:${USER} /home/$USER/.ldc/ldc2.conf

# RUN addgroup hexafoil 

COPY docker-entrypoint.sh .

RUN chown ${USER}:${USER} "/docker-entrypoint.sh"
RUN chmod +x "/docker-entrypoint.sh"


COPY zshrc /home/$USER/.zshrc
USER ${USER}
WORKDIR /home/${USER}



ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/bin/sh"]

