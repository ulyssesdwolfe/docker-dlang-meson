dlang-meson docker image
---

### URL : [docker-dlang-meson](https://bitbucket.org/ulyssesdwolfe/docker-dlang-meson)

### Description

Image Base: Alpine Linux - Latest

This image contains the latest build of the ldc2 compiler from [ldc releases page](https://github.com/ldc-developers/ldc/releases)
Also included is the Meson build tool, as well as other assorted build libs and helper tools.

### Directions

You can use this image in your docker file with the command 

    FROM ulyssesdwolfe/dlang-meson:latest

After building your image you can create an image with the following command

    docker container create -t -i \
    -v $(pwd):/home/meson-worker/<project-name> \
    --name <cont-name> ulyssesdwolfe/dlang-meson /bin/zsh && \ 
    docker container start <cont-name>

Then to run it 

    docker exec -u meson-worker -it <container-name> /bin/zsh

replace meson-worker with root to run as root

    docker exec -u root -it <container-name> /bin/zsh
 
dlang, d, alpine, meson, ldc2, ldc, dub
