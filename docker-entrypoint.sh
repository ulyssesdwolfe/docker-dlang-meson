#!/bin/zsh

set -e 

: ${DC:=ldc2}

export CC=${DC}

if [ "$1" = 'build' ]; then

    exec "/usr/bin/meson" $2 "${*[@]:3}"
fi

exec $@
