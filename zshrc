# Path to your oh-my-zsh installation.
# reposync

export ZSHRC=/home/$USER/.zshrc

autoload -Uz compinit promptinit
compinit
promptinit
prompt walters


SAVEHIST=500



# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/.config/zsh

alias vim=/usr/local/bin/nvim
alias ss-ag=/usr/bin/ag
